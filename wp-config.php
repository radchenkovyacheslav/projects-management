<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pm');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z-f1b6Vqra.%*XR]R87U>y9!tQJy(PDvJ)At55t{IhUe[M~u]$R$N`2z??ZZhiL0');
define('SECURE_AUTH_KEY',  'k!YbwW:<Polo>4GuC;KILoWaZA&-Jr(Nbb#>5}2p+0`LA#WY!pW9YgA!h$n@*y6!');
define('LOGGED_IN_KEY',    '8HziSk*M!H7SPiZp$SellvQ24%Qx) `.&h6{iL+xOBDP<Yhy?J`k8$=,WJq>{sqz');
define('NONCE_KEY',        '[T`.:K:(%Ld5->.6zlX%Sc_IwzTh%:&TXD3R +,V?ju1Olky!W<pI/F)mM5ab=&]');
define('AUTH_SALT',        'E;resNs2 KQEK;0hF3*9UP10>~Ebz((4F:#g~3C+AeYM1zB4JpQIvXjwEH-i/htq');
define('SECURE_AUTH_SALT', 'xATz]M5xH2NM2q7oB@^Qxy#gmu3sf^?z?5HWdRQ<8XM$ [noF=A%rl(B*|LKAOmT');
define('LOGGED_IN_SALT',   'j=ZY5~$fxZ6HiGI{gIRV {Id4f^2}IZ0v/pNZer~J(/*$H1lG~=zwsDB_n.VP|a$');
define('NONCE_SALT',       '3j-`wyTBh6Hs1z9 K60Y8EVS7dN#iTnqw -vkm+r9L~*Ce;scR8OukX.l$|Zf2jQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
