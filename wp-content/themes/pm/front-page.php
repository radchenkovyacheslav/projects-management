<?php

echo basename(__FILE__)."<br>";

if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
else { $paged = 1; }

//$paged = (get_query_var('page')) ? get_query_var('page') : 1;


get_header();


/*
$data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}posts WHERE  `ID` IN
(SELECT project_id FROM {$wpdb->prefix}projects_users WHERE user_id = '".get_current_user_id()."')");
*/

$user_role = get_user_meta(get_current_user_id(),"pm_role")[0];
$sql = "";

if($user_role != "admin"){
    $sql = "SELECT project_id FROM {$wpdb->prefix}projects_users WHERE user_id = ".get_current_user_id();
}else{
    $sql = "SELECT ID as project_id FROM {$wpdb->prefix}posts WHERE post_type = 'project'";
}
$data = $wpdb->get_results($sql,"ARRAY_N");

/*var_dump($sql);*/
//var_dump($data);

$data = array_map(function($n){
    return $n[0];
}, $data);
//var_dump($data);

$posts = query_posts(
    array(
            'post_type' => 'project',
            'post__in' =>  $data,
            'posts_per_page' => 2,
            'paged' => $paged
        )
);

foreach ($posts as $elem){
    ?>
        <a href="<?php echo get_permalink($elem->ID); ?>"><?php echo $elem->post_title ?></a><br>
    <?php
}



the_posts_pagination();

get_footer();

?>