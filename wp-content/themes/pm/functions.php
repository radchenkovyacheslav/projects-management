<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.03.2017
 * Time: 17:34
 */

include "include/ajax.php";
include "templates/main.php";
include "include/projects.php";
include "include/tasks.php";
include "include/comments.php";

/*
 * Добавляем дополнительные поля для пользователя
 * */
if (!function_exists("pm_edit_user_status")) {
    function pm_edit_user_status($user)
    {
        global $wpdb;
        $pm_role = get_user_meta($user->ID, 'pm_role', true);
        $roles = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}users_roles`");

        ?>
        <table class="form-table">
            <tr>
                <th><label for="user_active_status"><?php esc_html_e('User Role', 'pm'); ?></label></th>
                <td>
                    <select name="pm_role">
                        <?php
                        if ($roles && sizeof($roles)) {
                            foreach ($roles as $role) {
                                echo "<option " . ($role->type == $pm_role ? 'selected="selected"' : '') .
                                    "value='{$role->type}'>" . __($role->title, 'pm') . "</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
        </table>

        <?php

    }

    add_action('show_user_profile', 'pm_edit_user_status');
    add_action('edit_user_profile', 'pm_edit_user_status');
}


/*
Save new values for the user meta
*/
if (!function_exists('pm_save_user_meta')) {
    function pm_save_user_meta($user_id)
    {

        if (empty($_POST['pm_role'])) {
            // ????????????? delete_post_meta( $user_id, 'pm_role' );
        } else {
            update_user_meta($user_id, 'pm_role', sanitize_text_field($_POST['pm_role']));
        }
    }

    add_action('personal_options_update', 'pm_save_user_meta');
    add_action('edit_user_profile_update', 'pm_save_user_meta');
}



/*
 * Добавление дополнительных полей в проекты
 * */
if (!function_exists('project_meta_box')) {
    function project_meta_box()
    {
        add_meta_box(
            'project_meta_box', // Идентификатор(id)
            'Дополнительные поля проекта', // Заголовок области с мета-полями(title)
            'show_project_metabox', // Вызов(callback)
            'project', // Где будет отображаться наше поле, в нашем случае в разделе Красная Книга
            'normal',
            'high');

        add_meta_box(
            'project_users', // Идентификатор(id)
            'Пользователи', // Заголовок области с мета-полями(title)
            'show_project_users', // Вызов(callback)
            'project', // Где будет отображаться наше поле, в нашем случае в разделе Красная Книга
            'normal',
            'high');
    }

    add_action('add_meta_boxes', 'project_meta_box'); // Запускаем функцию
}


if (!function_exists("show_project_users")) {
    function show_project_users()
    {
        global $post;


// Выводим скрытый input, для верификации. Безопасность прежде всего!
        echo '<input type="hidden" name="user_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />';
        echo '<input type="hidden" id="projectid" value="' . $post->ID . '" />';

        // Начинаем выводить таблицу с полями через цикл
        echo '<table class="form-table">';

        // Получаем значение если оно есть для этого поля
        $meta = get_post_meta($post->ID, $field['id'], true);
        // Начинаем выводить таблицу
        echo '<tr>
                <th><label >Пользователи</label></th>
                <td id="out_of_project_users_list">';
        echo get_out_of_project_users($post->ID);

        //var_dump(get_users());
        ?>

        <?
        echo '</td>';
        echo '<td>';

        //  <input type="submit" name=""
        echo "
        
        ";
        echo '</td>';
        echo '<td id="users_list">';
        echo project_users_list($post->ID);
        echo '</td>';
        echo '</tr>';

        echo '</table>';
    }


}

if(!function_exists("get_out_of_project_users")){
    /**
     * @param $project_id - id проекта
     * @return mixed - html - список пользователей
     *
     * Функция возвращает список пользователей, которх нет в проекте
     */
    function get_out_of_project_users($project_id){

        global $wpdb;
        $sql = "SELECT DISTINCT user_id FROM {$wpdb->prefix}projects_users WHERE project_id = {$project_id}";
        $data = $wpdb->get_results($sql, "ARRAY_N");

        $data = array_map(function ($n) {
            return $n[0];
        }, $data);
        $result = wp_dropdown_users([
            'show_option_none' => 'не выбрано',
            'name' => 'userid',
            'id' => 'new_project_user',
            'exclude' => implode(",",$data),
            'echo' => 0,

        ]);

        return $result==""?"No users found.":$result.'<input type="button" id="addUser" value="addUser">';
    }
}

if (!function_exists('pm_custom_meta_boxes')) {
    function pm_custom_meta_boxes()
    {
        $pm_meta_fields = array(
            array(
                'label' => 'Тип',
                'desc' => 'Выберите тип.',
                'id' => 'select_type',
                'type' => 'select',
                'options' => array(  // Параметры, всплывающие данные
                    'one' => array(
                        'label' => 'Хордовые',  // Название поля
                        'value' => 'Хордовые'  // Значение
                    ),
                    'two' => array(
                        'label' => 'Беспозвоночные',  // Название поля
                        'value' => 'Беспозвоночные'  // Значение
                    ),
                    'three' => array(
                        'label' => 'Полухордовые',  // Название поля
                        'value' => 'Полухордовые'  // Значение
                    )
                )
            ),
            array(
                'label' => 'Класс',
                'desc' => 'Выберите класс.',
                'id' => 'select_class',
                'type' => 'select',
                'options' => array(  // Параметры, всплывающие данные
                    'one' => array(
                        'label' => 'Млекопитающие',  // Название поля
                        'value' => 'Млекопитающие'  // Значение
                    ),
                    'two' => array(
                        'label' => 'Насекомые',  // Название поля
                        'value' => 'Насекомые'  // Значение
                    ),
                    'three' => array(
                        'label' => 'Птицы',  // Название поля
                        'value' => 'Птицы'  // Значение
                    )
                )
            ),
            array(
                'label' => 'Отряд',
                'desc' => '',
                'id' => 'order', // даем идентификатор.
                'type' => 'text'  // Указываем тип поля.
            ),
            array(
                'label' => 'Вид',
                'desc' => '',
                'id' => 'kind',  // даем идентификатор.
                'type' => 'text'  // Указываем тип поля.
            ),
            array(
                'label' => 'Исчезающий или исчезнувший',
                'desc' => '',
                'id' => 'lost',
                'type' => 'select',
                'options' => array(  // Параметры, всплывающие данные
                    'one' => array(
                        'label' => 'Исчезающий',  // Название поля
                        'value' => 'Исчезающий'  // Значение
                    ),
                    'three' => array(
                        'label' => 'Исчезнувший',  // Название поля
                        'value' => 'Исчезнувший'  // Значение
                    )
                )
            )
        );

        return $pm_meta_fields;
    }


}


function show_project_metabox()
{
    $red_meta_fields = pm_custom_meta_boxes(); // Обозначим наш массив с полями глобальным
    global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
    echo '<input type="hidden" name="custom_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />';

    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';
    foreach ($red_meta_fields as $field) {
        // Получаем значение если оно есть для этого поля
        $meta = get_post_meta($post->ID, $field['id'], true);
        // Начинаем выводить таблицу
        echo '<tr>
                <th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th>
                <td>';
        switch ($field['type']) {
            // Текстовое поле
            case 'text':
                echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" />
					        <br /><span class="description">' . $field['desc'] . '</span>';
                break;
            // Список
            case 'select':
                echo '<select name="' . $field['id'] . '" id="' . $field['id'] . '">';
                foreach ($field['options'] as $option) {
                    echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="' . $option['value'] . '">' . $option['label'] . '</option>';
                }
                echo '</select><br /><span class="description">' . $field['desc'] . '</span>';
                break;
        }
        echo '</td></tr>';
    }
    echo '</table>';
}


if (!function_exists('save_project_meta_fields')) {
// Пишем функцию для сохранения
    function save_project_meta_fields($post_id)
    {
        $meta_fields = pm_custom_meta_boxes();  // Массив с нашими полями

        // проверяем наш проверочный код
        if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
            return $post_id;
        // Проверяем авто-сохранение
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;
        // Проверяем права доступа
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        // Если все отлично, прогоняем массив через foreach
        foreach ($meta_fields as $field) {
            $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
            $new = $_POST[$field['id']];
            if ($new && $new != $old) {  // Если данные новые
                update_post_meta($post_id, $field['id'], $new); // Обновляем данные
            } elseif ('' == $new && $old) {
                delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
            }
        } // end foreach
    }

    add_action('save_post', 'save_project_meta_fields'); // Запускаем функцию сохранения
}


//function my_admin_init(){

add_role('client', __('Client'),

    array(

        'read' => true, // true allows this capability
        'edit_posts' => true, // Allows user to edit their own posts
        'edit_pages' => true, // Allows user to edit pages
        'edit_others_posts' => true, // Allows user to edit others posts not just their own
        'create_posts' => true, // Allows user to create new posts
        'manage_categories' => true, // Allows user to manage post categories
        'publish_posts' => true, // Allows the user to publish, otherwise posts stays in draft mode
        'edit_themes' => false, // false denies this capability. User can’t edit your theme
        'install_plugins' => false, // User cant add new plugins
        'update_plugin' => false, // User can’t update any plugins
        'update_core' => false, // user cant perform core updates


    )

);
$result = get_role('client');
$result->add_cap('my_acl');// current_user_can("my_acl")

/*
Do not allow backend to non authorized users
*/
if (!function_exists('pm_non_admin_users')) {
    add_action('admin_init', 'pm_non_admin_users');
    function pm_non_admin_users()
    {
        //$user_ID    = get_current_user_id();
        //$user_agent = get_user_meta( $user_ID, 'user_agent', true );
        $caps = get_user_meta(get_current_user_id(), 'wp_capabilities', true);
        $roles = array_keys((array)$caps);

        if (!isset($caps["administrator"]) && !stristr($_SERVER['PHP_SELF'], 'admin-ajax.php')) {
            wp_redirect(home_url());
            exit;
        }
    }
}


//
//}

//add_action("load-themes.php", "my_admin_init");


/*
    Do not allow frontend to non authorized users
*/
if (!function_exists('redirect_guests')) {
    add_action('init', 'redirect_guests');
    function redirect_guests()
    {

        if (!is_user_logged_in() && !stristr($_SERVER['PHP_SELF'], 'wp-login.php')) {
            //wp_redirect("/wp-login.php");
            auth_redirect();
            exit();
        }

    }
}


// удаляет H2 из шаблона пагинации
if (!function_exists(my_navigation_template)) {
    add_filter('navigation_markup_template', 'my_navigation_template', 10, 2);
    function my_navigation_template($template, $class)
    {
        return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
    }
}

if (!function_exists(show_404)) {
    function show_404()
    {
        global $wp_query;
        $wp_query->set_404();
        status_header(404);
        get_template_part(404);
        exit();
    }
}

add_action( 'wp_enqueue_scripts', 'add_scripts' );
function add_scripts(){
    wp_deregister_script("jquery");
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js', false, null, true);
    wp_enqueue_script( 'jquery');
}
