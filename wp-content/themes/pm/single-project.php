<?php



$user_role = get_user_meta(get_current_user_id(),"pm_role")[0];

if($user_role != "admin"){
    $sql = "SELECT * FROM {$wpdb->prefix}projects_users 
                  WHERE user_id = ".get_current_user_id()." AND project_id=".get_the_ID();
    $data = $wpdb->get_results($sql);
    // если пост
    if(!sizeof($data)){
        show_404();
    }

}

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();

?>

<h1><?php the_title(); ?></h1>
    <p><?php the_content(); ?></p>

<?php

endwhile;
endif;

comments_template( /*'/templates/comments.php'*/'', true );


get_footer();