<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.04.2017
 * Time: 13:46
 */

function get_post_type_template($single_template) {
    global $post;

    if ($post->post_type == 'my_post_type') {
        $single_template = dirname( __FILE__ ) . 'templates/post-type-template.php';
    }
    return $single_template;
}
add_filter( 'single_template', 'get_post_type_template' );