<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.04.2017
 * Time: 10:55
 */


// создание формы
if(!function_exists('image_comment_form')) {
    function image_comment_form(){
        echo '<p style="clear: both"><input style="width: auto" type="file" name="image[]" multiple/>'.__("Image", "pm").'</p>';
    }
    add_action('comment_form_after_fields', 'image_comment_form');
    add_action( 'comment_form_logged_in_after', 'image_comment_form');
}


add_filter( 'comment_text', 'modify_extend_comment');
function modify_extend_comment( $text ){



    $plugin_url_path = WP_PLUGIN_URL;
    $comment_id = get_comment_ID();


    if( $imageid = get_comment_meta($comment_id , '_images', true ) ) {
        $text .= "<br>".wp_get_attachment_image( $imageid);
        //$commenttitle = '<strong>' . esc_attr( $commenttitle ) . '</strong><br/>';
        //$text = $commenttitle . $text;
    }

    if( $commentrating = get_comment_meta( get_comment_ID(), 'rating', true ) ) {
        $commentrating = '<p class="comment-rating">  <img src="'. $plugin_url_path .
            '/extend-comment/images/'. $commentrating . 'star.gif"/><br/>Rating: <strong>'. $commentrating .' / 5</strong></p>';
        $text = $text . $commentrating;
        return $text;
    } else {
        return $text;
    }
}


//сохранение результата
if(!function_exists('save_image')) {
    function save_image($comment_ID){
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        if(isset($_FILES["image"])){
           // foreach ($_FILES["image"] as $k=>$v)
            $attachment_id = media_handle_upload( "image", 0);
            update_comment_meta( $comment_ID, "_images", $attachment_id);
        }
     }
    add_action('comment_post', 'save_image', 99);
}


if(!function_exists('add_script')){
    function add_script(){
        ?>
        <script>
            // меняет тип кодировки формы. передача файлов на сервер
              $("form.comment-form").attr("enctype","multipart/form-data");
        </script>
        <?php
    }
    add_action('wp_footer', 'add_script',99);

}

