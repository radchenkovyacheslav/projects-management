<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.04.2017
 * Time: 12:35
 */

// или так с WP 4.7
if (wp_doing_ajax()) {
    add_action('wp_ajax_add-project-user', 'add_project_user');
    add_action('wp_ajax_delete-project-user', 'delete_project_user');
}

if (!function_exists("add_project_user")) {
    function add_project_user()
    {
        global $wpdb;

        // $nonce = $_POST['nonce'];

        // проверяем nonce код, если проверка не пройдена прерываем обработку
        //  if (!wp_verify_nonce($nonce, 'adduser'))
        //     die('Stop!');

        $user_id = $_POST["userid"];
        $project_id = $_POST["projectid"];

        $wpdb->insert($wpdb->prefix . "projects_users", ['user_id' => $user_id,
            'project_id' => $project_id]);

        echo json_encode([
                           get_out_of_project_users($project_id),
                           project_users_list($project_id)
                         ]);
        wp_die();
    }


}

add_action('admin_print_footer_scripts', 'add_user_script'); // для фронта

if (!function_exists("add_user_script")) {
    function add_user_script()
    {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $("#out_of_project_users_list").on("click", "#addUser",function(){


                    var data = {
                        action: 'add-project-user',
                        userid: $("#new_project_user").val(),
                        projectid: $("#projectid").val(),
                    };

                    $("#out_of_project_users_list").html("<img src='"+"<?=get_template_directory_uri()?>"+"/img/ajax-loader.gif'>");
                    $("#users_list").html("<img src='"+"<?=get_template_directory_uri()?>"+"/img/ajax-loader.gif'>");

                    if(data.userid < 1){
                        alert("Укажите пользователя");
                        return;
                    }
                    // 'ajaxurl' не определена во фронте, поэтому мы добавили её аналог с помощью wp_localize_script()
                    jQuery.post("<?=admin_url("admin-ajax.php")?>", data, function (response) {
                        //alert('Получено с сервера: ' + response);
                        $("#out_of_project_users_list").html(response[0]);
                        $("#users_list").html(response[1]);
                        // привязываем собятие нажатия к ссылкам Delete
                        $("[data-userid]").click(deleteUser);
                    },"json");
                });

                function deleteUser(event) {

                    event.preventDefault();
                    var data = {
                        action: 'delete-project-user',
                        userid: $(this).data("userid"),
                        projectid: $(this).data("projectid"),
                    };

                    $("#out_of_project_users_list").html("<img src='"+"<?=get_template_directory_uri()?>"+"/img/ajax-loader.gif'>");
                    $("#users_list").html("<img src='"+"<?=get_template_directory_uri()?>"+"/img/ajax-loader.gif'>");


                    jQuery.post("<?=admin_url("admin-ajax.php")?>", data, function (response) {
                        //alert('Получено с сервера: ' + response);
                        $("#out_of_project_users_list").html(response[0]);
                        $("#users_list").html(response[1]);
                        // привязываем собятие нажатия к ссылкам Delete
                        $("[data-userid]").click(deleteUser);
                    }, "json")
                        .fail(function() {
                            alert( "error" );
                        });

                }

                $("[data-userid]").click(deleteUser);


            });





        </script>
        <?
    }
}

if(!function_exists('delete_project_user')){
    function delete_project_user(){
        global $wpdb;

        // $nonce = $_POST['nonce'];

        // проверяем nonce код, если проверка не пройдена прерываем обработку
        //  if (!wp_verify_nonce($nonce, 'adduser'))
        //     die('Stop!');

        $user_id = $_POST["userid"];
        $project_id = $_POST["projectid"];


        $result = $wpdb->delete( $wpdb->prefix . "projects_users",
            array( 'user_id' => $user_id, 'project_id' => $project_id),
            array( '%d','%d' ) );

        if($result === false){
            echo json_encode(["Delete error."]);
            wp_die();

        }

        echo $res = json_encode([
            get_out_of_project_users($project_id),
            project_users_list($project_id)
        ]);
        wp_die();
    }
}

if(!function_exists('project_users_list')){
    function project_users_list($project_id){
        global $wpdb;

        ob_start();
        ?>
        <ul>
            <?php
            // получить всех пользователей проекта
            $users = $wpdb->get_col("SELECT user_id FROM " . $wpdb->prefix . "projects_users".
                                      " WHERE project_id='{$project_id}'");

            if(sizeof($users)) {

                $args = array(
                    'include' => $users,
                );
                $users = get_users($args);


                foreach ($users as $user) {
                    echo "<li>";

                    echo $user->user_nicename;
                    echo "<a href='#' style='text-decoration:none; color:red' data-userid='" . $user->ID . "' data-projectid='{$project_id}'><i class='dashicons dashicons-no-alt'></i></a>";
                    echo "</li>";
                }
            }else{
                echo __("No users found", "pm");
                return ob_get_clean();
            }

            ?>
        </ul>
        <?
        return ob_get_clean();


    }
}